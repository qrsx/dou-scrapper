package cities

import "gitlab.com/qrsx/dou-scrapper/models"

var (
	Kyiv               = models.City{"kyiv", "Київ"}
	Kharkiv            = models.City{"kharkiv", "Харків"}
	Odessa             = models.City{"odessa", "Одеса"}
	Lviv               = models.City{"lviv", "Львів"}
	Rivne              = models.City{"rivne", "Рівне"}
	Dnipro             = models.City{"dnipro", "Дніпро"}
	Zaporizhia         = models.City{"zaporizhia", "Запоріжжя"}
	Kremenchuk         = models.City{"kremenchuk", "Кременчук"}
	Mykolaiv           = models.City{"mykolaiv", "Миколаїв"}
	Poltava            = models.City{"poltava", "Полтава"}
	Kramatorsk         = models.City{"kramatorsk", "Краматорськ"}
	Mariupol           = models.City{"mariupol", "Маріуполь"}
	Chernihiv          = models.City{"chernihiv", "Чернігів"}
	Kherson            = models.City{"kherson", "Херсон"}
	Uzhhorod           = models.City{"uzhhorod", "Ужгород"}
	Cherkasy           = models.City{"cherkasy", "Черкаси"}
	Vinnica            = models.City{"vinnica", "Вінниця"}
	Chernivtsi         = models.City{"chernivtsi", "Чернівці"}
	IvanoFrankivsk     = models.City{"ivano-frankivsk", "Івано-Франківськ"}
	Khmelnytskyi       = models.City{"khmelnytskyi", "Хмельницький"}
	Zhitomir           = models.City{"zhitomir", "Житомир"}
	Lutsk              = models.City{"lutsk", "Луцьк"}
	Ternopil           = models.City{"ternopil", "Тернопіль"}
	Sumy               = models.City{"sumy", "Суми"}
	Kropyvnytskyi      = models.City{"kropyvnytskyi", "Кропивницький"}
	KamianetsPodilskyi = models.City{"kamianets-podilskyi", "Кам’янець-Подільський"}
	Mukachevo          = models.City{"mukachevo", "Мукачево"}
	Boyarka            = models.City{"boyarka", "Боярка"}
	Brovary            = models.City{"brovary", "Бровари"}
	Kalush             = models.City{"kalush", "Калуш"}
	Berdyansk          = models.City{"berdyansk", "Бердянськ"}
	Kamianske          = models.City{"kamianske", "Кам'янське"}
	KryvyiRih          = models.City{"kryvyi-rih", "Кривий Ріг"}
)

var cityNameMap map[string]models.City

func init() {
	original := classified()
	additional := additional()

	cityNameMap = make(map[string]models.City, len(original)+len(additional))

	for _, city := range original {
		cityNameMap[city.Name] = city
	}

	for name, city := range additional {
		cityNameMap[name] = city
	}
}

func FindCitiesByNames(names []string) []models.City {
	result := make([]models.City, len(names))

	for i, name := range names {
		result[i] = FindCityByName(name)
	}

	return result
}

func FindCityByName(name string) models.City {
	if result, ok := cityNameMap[name]; ok {
		return result
	}

	return models.City{Name: name}
}

func additional() map[string]models.City {
	return map[string]models.City{
		"Киев":            Kyiv,
		"Kiev":            Kyiv,
		"Харьков":         Kharkiv,
		"Львов":           Lviv,
		"Днепр":           Dnipro,
		"Одесса":          Odessa,
		"Винница":         Vinnica,
		"Запорожье":       Zaporizhia,
		"Черновцы":        Chernivtsi,
		"Ивано-Франковск": IvanoFrankivsk,
		"Ужгород":         Uzhhorod,
		"Хмельницкий":     Khmelnytskyi,
		"Херсон":          Kherson,
		"Черкассы":        Cherkasy,
		"Николаев":        Mykolaiv,
		"Житомир":         Zhitomir,
		"Чернигов":        Chernihiv,
		"Луцк":            Lutsk,
		"Тернополь":       Ternopil,
		"Сумы":            Sumy,
		"Ровно":           Rivne,
		"Мариуполь":       Mariupol,
		"Полтава":         Poltava,
		"Кременчуг":       Kremenchuk,
		"Краматорск":      Kramatorsk,
		"Кропивницкий":    Kropyvnytskyi,
		"Мукачево":        Mukachevo,
		"Боярка":          Boyarka,
		"Бровары":         Brovary,
		"Бердянск":        Berdyansk,
		"Каменское":       Kamianske,
		"Кривой Рог":      KryvyiRih,
	}
}

func classified() []models.City {
	return []models.City{
		Kyiv,
		Kharkiv,
		Odessa,
		Lviv,
		Rivne,
		Dnipro,
		Zaporizhia,
		Kremenchuk,
		Mykolaiv,
		Poltava,
		Kramatorsk,
		Mariupol,
		Chernihiv,
		Kherson,
		Uzhhorod,
		Cherkasy,
		Vinnica,
		Chernivtsi,
		IvanoFrankivsk,
		Khmelnytskyi,
		Zhitomir,
		Lutsk,
		Ternopil,
		Sumy,
		Kropyvnytskyi,
		KamianetsPodilskyi,
		Mukachevo,
		Boyarka,
		Brovary,
		Kalush,
		Berdyansk,
		Kamianske,
		KryvyiRih,
	}
}
