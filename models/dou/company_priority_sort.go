package dou

import "sort"

type CompanyPrioritySortList struct {
	list          []FullCompany
	aliasPriority map[string]uint32
}

func NewCompanyPrioritySortList(list []FullCompany, aliasPriority map[string]uint32) sort.Interface {
	return &CompanyPrioritySortList{list: list, aliasPriority: aliasPriority}
}

func (l *CompanyPrioritySortList) Len() int {
	return len(l.list)
}

func (l *CompanyPrioritySortList) Less(i, j int) bool {
	iPriority := l.aliasPriority[l.list[i].Alias]
	jPriority := l.aliasPriority[l.list[j].Alias]

	return iPriority >= jPriority
}

func (l *CompanyPrioritySortList) Swap(i, j int) {
	l.list[i], l.list[j] = l.list[j], l.list[i]
}
