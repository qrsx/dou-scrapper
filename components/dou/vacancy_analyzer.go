package dou

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/juju/errors"
	"gitlab.com/qrsx/dou-scrapper/components/goquery"
	"gitlab.com/qrsx/dou-scrapper/components/logger"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
)

var (
	errNoneVacancyUrl = errors.New("none vacancy url")
)

func analyzePaginationVacancySet(result []dou.PaginationVacancy, urlImportedMap map[string]uint32, now uint32, document *goquery.Document) ([]dou.PaginationVacancy, dou.AnalyzeStats) {
	vacancyListSelector := document.Find("li.l-vacancy .vacancy .title")

	errorCount := 0
	newestCount := 0

	vacancyListSelector.Each(func(i int, vacancySelector *goquery.Selection) {
		title := vacancySelector.Find("a.vt")

		sourceVacancyURL, exists := title.Attr("href")
		if !exists {
			errorCount += 1

			logger.Error(errors.Trace(errNoneVacancyUrl))

			return
		}

		vacancyURL, hot := parseVacancyUrl(sourceVacancyURL)

		imported, exists := urlImportedMap[vacancyURL]
		if !exists {
			imported = now
			newestCount += 1
		}

		sourceCities := scrapper.Split(vacancySelector.Find(".cities").Text())
		cities, remote := extractRemoteCity(sourceCities)

		result = append(result, dou.PaginationVacancy{
			Url:      vacancyURL,
			Title:    title.Text(),
			Salary:   vacancySelector.Find(".salary").Text(),
			Cities:   cities,
			Imported: imported,
			Remote:   remote,
			Hot:      hot,
		})
	})

	return result, dou.AnalyzeStats{
		TotalCount:  vacancyListSelector.Length(),
		NewestCount: newestCount,
		ErrorCount:  errorCount,
	}
}
