package dou

import (
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"gitlab.com/qrsx/dou-scrapper/models/protos/dou/storage"
	"io/ioutil"
	"os"
	"time"
)

type VacancyStorage struct {
	path     string
	lifetime uint32
}

func NewVacancyStorage(path string, lifetime uint32) *VacancyStorage {
	return &VacancyStorage{path: path, lifetime: lifetime}
}

func (s *VacancyStorage) Fetch() ([]dou.PaginationVacancy, bool, error) {
	content, err := ioutil.ReadFile(s.path)

	if err != nil {
		if os.IsNotExist(err) {
			return nil, false, nil
		}

		return nil, false, err

	}

	message := storage.Vacancies{}

	err = message.Unmarshal(content)
	if err != nil {
		return nil, false, err
	}

	expired := uint32(time.Now().Unix())-s.lifetime > message.Updated
	vacancies := message.Vacancies
	result := make([]dou.PaginationVacancy, len(vacancies))
	for i, v := range vacancies {
		result[i] = dou.PaginationVacancy{
			Url:      v.Url,
			Title:    v.Title,
			Salary:   v.Salary,
			Cities:   v.Cities,
			Imported: v.Imported,
			Remote:   v.Remote,
			Hot:      v.Hot,
		}
	}

	return result, expired, nil
}

func (s *VacancyStorage) Store(source []dou.PaginationVacancy) error {
	vacancies := make([]*storage.Vacancy, len(source))
	for i, v := range source {
		vacancies[i] = &storage.Vacancy{
			Url:      v.Url,
			Title:    v.Title,
			Salary:   v.Salary,
			Cities:   v.Cities,
			Imported: v.Imported,
			Remote:   v.Remote,
			Hot:      v.Hot,
		}
	}

	result := storage.Vacancies{
		Vacancies: vacancies,
		Updated:   uint32(time.Now().Unix()),
	}

	content, err := result.Marshal()

	if err != nil {
		return err
	}

	return ioutil.WriteFile(s.path, content, 0666)
}
