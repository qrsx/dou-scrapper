module gitlab.com/qrsx/dou-scrapper

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/gogo/protobuf v1.3.1
	github.com/juju/errors v0.0.0-20200330140219-3fe23663418f
	github.com/juju/testing v0.0.0-20201216035041-2be42bba85f3 // indirect
	github.com/mailru/easyjson v0.7.1
	github.com/stretchr/testify v1.6.1
	github.com/valyala/fasthttp v1.14.0
)
