package dou

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"os"
	"path/filepath"
	"testing"
)

const (
	vacancyStoragePath = "/var/proto.storage/dou/vacancy-test.storage"
)

func TestVacancyStorage_FetchNotExist(t *testing.T) {
	if testing.Short() {
		return
	}

	path, err := filepath.Abs(vacancyStoragePath)
	if !assert.NoError(t, err) {
		return
	}

	err = os.Remove(path)
	if err != nil && !os.IsNotExist(err) {
		assert.NoError(t, err)

		return
	}

	storage := NewVacancyStorage(path, 3600)

	data, expired, err := storage.Fetch()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, []dou.PaginationVacancy(nil), data)
	assert.Equal(t, false, expired)
}

func TestVacancyStorage_Store(t *testing.T) {
	if testing.Short() {
		return
	}

	path, err := filepath.Abs(vacancyStoragePath)
	if !assert.NoError(t, err) {
		return
	}

	storage := NewVacancyStorage(path, 3600)

	// store empty
	err = storage.Store(nil)
	if !assert.NoError(t, err) {
		return
	}

	vacancies := []dou.PaginationVacancy{
		{
			Url:      "https://jobs.dou.ua/companies/devspace/vacancies/100000/",
			Title:    "Junior Golang Developer",
			Salary:   "$2500-$5000",
			Cities:   []string{"Київ", "Львів"},
			Imported: 1571063700,
			Remote:   false,
			Hot:      false,
		},
		{
			Url:      "https://jobs.dou.ua/companies/devspace/vacancies/100001/",
			Title:    "Golang Developer",
			Salary:   "$3500-$6000",
			Cities:   []string{"Київ", "Львів"},
			Imported: 1571063800,
			Remote:   true,
			Hot:      true,
		},
	}

	// store any
	err = storage.Store(vacancies)
	if !assert.NoError(t, err) {
		return
	}

	// assert exists
	data, expired, err := storage.Fetch()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, vacancies, data)
	assert.Equal(t, false, expired)
}
