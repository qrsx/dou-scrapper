package dou

import (
	"github.com/juju/errors"
	"gitlab.com/qrsx/dou-scrapper/components/goquery"
	"gitlab.com/qrsx/dou-scrapper/components/logger"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"strconv"
)

const (
	maxExpectedReviewCount          = 16384
	companyWithReviewsExpectedCount = 1024
)

type ReviewScrapper struct {
	storage *ReviewStorage
	client  *Client
}

func NewReviewScrapper(storage *ReviewStorage, client *Client) *ReviewScrapper {
	return &ReviewScrapper{storage: storage, client: client}
}

func (s *ReviewScrapper) FullScrap() (map[string]int, dou.ScrapStats, error) {
	stored, expired, err := s.storage.Fetch()
	if err != nil {
		return nil, dou.ScrapStats{}, err
	}

	if len(stored) > 0 && expired == false {
		return companyAliasReviewCountMap(stored), dou.ScrapStats{}, nil
	}

	stats := dou.ScrapStats{}
	stats.RequestCount += 1

	response, err := s.client.ReviewsMainPage()
	if err != nil {
		return nil, stats, err
	}

	document, err := scrapper.Document(response)
	if err != nil {
		return nil, stats, err
	}

	pages, err := strconv.Atoi(document.Find(".b-paging .page").Last().Find("a").Text())
	if err != nil {
		return nil, stats, err
	}

	storedUrlMap := reviewUrlMap(stored)

	result := make([]dou.PaginationReview, 0, maxExpectedReviewCount)

	result, analyzeStats := analyzePaginationReviewSet(result, document, storedUrlMap)
	stats.NewestCount += analyzeStats.NewestCount
	stats.ErrorCount += analyzeStats.ErrorCount

	for page := 2; page <= pages; page++ {
		response, err := s.client.ReviewsByPage(page)
		stats.RequestCount += 1

		if err != nil {
			logger.Error(errors.Trace(err))

			break
		}

		document, err := scrapper.Document(response)
		if err != nil {
			logger.Error(errors.Trace(err))

			break
		}

		result, analyzeStats = analyzePaginationReviewSet(result, document, storedUrlMap)
		stats.NewestCount += analyzeStats.NewestCount
		stats.ErrorCount += analyzeStats.ErrorCount
	}

	err = s.storage.Store(result)
	if err != nil {
		logger.Error(errors.Trace(err))

		// NOP, only logged
	}

	return companyAliasReviewCountMap(result), stats, nil
}

func (s *ReviewScrapper) DiffScrap() (map[string]int, dou.ScrapStats, error) {
	stored, expired, err := s.storage.Fetch()
	if err != nil {
		return nil, dou.ScrapStats{}, err
	}

	if len(stored) > 0 && expired == false {
		return companyAliasReviewCountMap(stored), dou.ScrapStats{}, nil
	}

	stats := dou.ScrapStats{}
	stats.RequestCount += 1

	response, err := s.client.ReviewsMainPage()
	if err != nil {
		return nil, stats, err
	}

	document, err := scrapper.Document(response)
	if err != nil {
		return nil, stats, err
	}

	pages, err := strconv.Atoi(document.Find(".b-paging .page").Last().Find("a").Text())
	if err != nil {
		return nil, stats, err
	}

	storedUrlMap := reviewUrlMap(stored)

	result := make([]dou.PaginationReview, 0, maxExpectedReviewCount)

	result, analyzeStats := analyzePaginationReviewSet(result, document, storedUrlMap)
	stats.NewestCount += analyzeStats.NewestCount
	stats.ErrorCount += analyzeStats.ErrorCount

	if analyzeStats.NewestCount == 0 {
		err = s.storage.Store(stored)
		if err != nil {
			logger.Error(errors.Trace(err))

			// NOP, only logged
		}

		return companyAliasReviewCountMap(stored), stats, nil
	}

	for page := 2; page <= pages; page++ {
		response, err := s.client.ReviewsByPage(page)
		stats.RequestCount += 1

		if err != nil {
			logger.Error(errors.Trace(err))

			break
		}

		document, err := scrapper.Document(response)
		if err != nil {
			logger.Error(errors.Trace(err))

			break
		}

		result, analyzeStats = analyzePaginationReviewSet(result, document, storedUrlMap)
		stats.NewestCount += analyzeStats.NewestCount
		stats.ErrorCount += analyzeStats.ErrorCount

		if analyzeStats.NewestCount == 0 {
			break
		}
	}

	merged := mergeReviewsDiff(result, stored)

	err = s.storage.Store(merged)
	if err != nil {
		logger.Error(errors.Trace(err))

		// NOP, only logged
	}

	return companyAliasReviewCountMap(merged), stats, nil
}

func companyAliasReviewCountMap(reviews []dou.PaginationReview) map[string]int {
	result := make(map[string]int, companyWithReviewsExpectedCount)

	for _, review := range reviews {
		result[review.CompanyAlias] += 1
	}

	return result
}

func reviewUrlMap(reviews []dou.PaginationReview) map[string]bool {
	result := make(map[string]bool, len(reviews))

	for _, review := range reviews {
		result[review.Url] = true
	}

	return result
}

func mergeReviewsDiff(newest, stored []dou.PaginationReview) []dou.PaginationReview {
	result := newest

	existsMap := make(map[string]bool, len(newest))
	for _, item := range newest {
		existsMap[item.Url] = true
	}

	for _, item := range stored {
		if !existsMap[item.Url] {
			result = append(result, item)
		}
	}

	return result
}
