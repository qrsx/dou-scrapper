package dou

import (
	"gitlab.com/qrsx/dou-scrapper/models"
	"gitlab.com/qrsx/dou-scrapper/models/gmap"
)

type (
	FullCompany struct {
		Alias          string
		Name           string
		Logo           string
		SiteURL        string
		Offices        []FullOffice
		Vacancies      []FullVacancy
		ReviewCount    int
		EmployeeCount  string
		CompanyType    string
		InTop50Largest bool
		PhotoExists    bool
	}

	FullOffice struct {
		City     models.City
		Address  string
		Location gmap.Location
	}

	FullVacancy struct {
		Id           uint64
		Title        string
		Cities       []models.City
		ExistsOffice bool
		Salary       string
		Published    string
		Remote       bool
	}
)
