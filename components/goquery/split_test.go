package scrapper

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSplit(t *testing.T) {
	assert.Equal(t, []string{"Київ", "Львів"}, Split("Київ, Львів"))
}

func BenchmarkSplit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = Split("Київ, Львів")
	}
}
