package dou

type SkipCompany struct {
	Alias        string
	Name         string
	VacancyCount int
	CityCount    int
	OfficeCount  int
}
