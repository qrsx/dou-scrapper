package dou

import (
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
)

func analyzePaginationReviewSet(result []dou.PaginationReview, document *goquery.Document, storedUrlMap map[string]bool) ([]dou.PaginationReview, dou.AnalyzeStats) {
	stats := dou.AnalyzeStats{}

	reviewSelectorlist := document.Find("#commentsList > .b-comment > .comment > .b-post-author")

	stats.TotalCount = reviewSelectorlist.Length()

	reviewSelectorlist.Each(func(i int, selection *goquery.Selection) {
		reviewUrlSelection := selection.Find("a").Last()
		reviewUrl, reviewUrlExists := reviewUrlSelection.Attr("href")

		companyUrlSelection := reviewUrlSelection.Prev()
		companyUrl, companyUrlExists := companyUrlSelection.Attr("href")

		companyAlias := extractCompanyAlias(companyUrl)

		if reviewUrlExists && companyUrlExists && reviewUrl != "" && companyAlias != "" {
			result = append(result, dou.PaginationReview{
				Url:          reviewUrl,
				CompanyAlias: companyAlias,
			})

			if !storedUrlMap[reviewUrl] {
				stats.NewestCount += 1
			}
		} else {
			stats.ErrorCount += 1
		}
	})

	return result, stats
}
