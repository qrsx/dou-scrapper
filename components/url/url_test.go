package url

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRemoveQuery(t *testing.T) {
	assert.Equal(
		t,
		"https://jobs.dou.ua/companies/devspace/vacancies/1000000/",
		RemoveQuery("https://jobs.dou.ua/companies/devspace/vacancies/1000000/?hot=1"),
	)
}

func BenchmarkRemoveQuery(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = RemoveQuery("https://jobs.dou.ua/companies/devspace/vacancies/1000000/?hot=1")
	}
}
