package dou

import (
	"gitlab.com/qrsx/dou-scrapper/components/cities"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"gitlab.com/qrsx/dou-scrapper/models/gmap"
	"sort"
	"strconv"
	"time"
)

func Combine(
	companies map[string]dou.Company,
	vacancies []dou.PaginationVacancy,
	companyReviewCountMap map[string]int,
	companyAliasTypeMap map[string]string,
	addressLocationMap map[string]gmap.Location,
	verifyCompanyMap map[string]bool,
) ([]dou.FullCompany, []dou.SkipCompany) {
	topLargestCompanyAliasMap := Top50LargestCompanyAliasMap()

	sort.Sort(dou.NewVacancySalaryExistsSortList(vacancies))

	companyCount := len(companies)

	companyAliasVacanciesMap := createCompanyAliasVacanciesMap(vacancies, companyCount)

	fullCompanies := make([]dou.FullCompany, 0, companyCount)
	skipCompanies := make([]dou.SkipCompany, 0)

	companyAliasPriorityMap := make(map[string]uint32, companyCount)

	cityCounter := cities.NewCounter()

	for _, company := range companies {
		vacancies := companyAliasVacanciesMap[company.Alias]

		sourceOfficeCount := len(company.Offices)
		offices := make([]dou.FullOffice, 0, sourceOfficeCount)
		for _, office := range company.Offices {
			address, skip := addressByOffice(office)
			if skip {
				continue
			}

			location, exists := addressLocationMap[address]

			if exists {
				offices = append(offices, dou.FullOffice{
					City:     cities.FindCityByName(office.City),
					Address:  office.Address,
					Location: location,
				})
			}
		}

		if len(offices) == 0 {
			cityCounter.Reset()
			for _, office := range company.Offices {
				cityCounter.Add(office.City)
			}

			skipCompanies = append(skipCompanies, dou.SkipCompany{
				Alias:        company.Alias,
				Name:         company.Name,
				VacancyCount: len(vacancies),
				CityCount:    cityCounter.Count(),
				OfficeCount:  sourceOfficeCount,
			})

			continue
		}

		reviewCount, reviewExists := companyReviewCountMap[company.Alias]
		fullCompany := dou.FullCompany{
			Alias:          company.Alias,
			Name:           company.Name,
			Logo:           company.Logo,
			SiteURL:        company.SiteUrl,
			Offices:        offices,
			Vacancies:      combineFullVacancies(vacancies, createOfficeCityMap(offices)),
			ReviewCount:    reviewCount,
			EmployeeCount:  company.EmployeeCount,
			CompanyType:    companyAliasTypeMap[company.Alias],
			InTop50Largest: topLargestCompanyAliasMap[company.Alias],
			PhotoExists:    company.PhotoExists,
		}
		fullCompanies = append(fullCompanies, fullCompany)

		companyAliasPriorityMap[company.Alias] = createCompanyPriority(
			companyHasVacancyInUkraine(fullCompany),
			reviewExists,
			company.PhotoExists,
			vacanciesWithSalaryCount(fullCompany.Vacancies),
			len(fullCompany.Vacancies),
			verifyCompanyMap[company.Alias],
		)
	}

	sort.Sort(dou.NewCompanyPrioritySortList(fullCompanies, companyAliasPriorityMap))
	sort.Sort(dou.SkipCompanySortList(skipCompanies))

	return fullCompanies, skipCompanies
}

func createCompanyAliasVacanciesMap(vacancies []dou.PaginationVacancy, size int) map[string][]dou.PaginationVacancy {
	result := make(map[string][]dou.PaginationVacancy, size)

	for _, vacancy := range vacancies {
		companyAlias := extractCompanyAlias(vacancy.Url)

		result[companyAlias] = append(result[companyAlias], vacancy)
	}

	return result
}

func createCompanyAliasMap(companies []dou.Company) map[string]dou.Company {
	result := make(map[string]dou.Company, len(companies))

	for _, company := range companies {
		result[company.Alias] = company
	}

	return result
}

func combineFullVacancies(vacancies []dou.PaginationVacancy, cityExistsMap map[string]bool) []dou.FullVacancy {
	result := make([]dou.FullVacancy, len(vacancies))

	for i, vacancy := range vacancies {
		id, _ := strconv.ParseUint(extractVacancyAlias(vacancy.Url), 10, 64)
		published := time.Unix(int64(vacancy.Imported), 0).Format("2006-01-02")

		result[i] = dou.FullVacancy{
			Id:           id,
			Title:        vacancy.Title,
			Cities:       cities.FindCitiesByNames(vacancy.Cities),
			ExistsOffice: inCityMap(cityExistsMap, vacancy.Cities),
			Salary:       vacancy.Salary,
			Published:    published,
			Remote:       vacancy.Remote,
		}
	}

	return result
}

func vacanciesWithSalaryCount(vacancies []dou.FullVacancy) int {
	result := 0

	for i := range vacancies {
		empty := vacancies[i].Salary == ""

		if empty {
			break
		}

		// sorted by salary exists
		result += 1
	}

	return result
}

func companyHasVacancyInUkraine(company dou.FullCompany) bool {
	cityMap := make(map[string]bool, len(company.Offices))

	for _, office := range company.Offices {
		cityAlias := office.City.Alias
		if cityAlias == "" {
			continue
		}

		cityMap[cityAlias] = true
	}

	for _, vacancy := range company.Vacancies {
		for _, city := range vacancy.Cities {
			cityAlias := city.Alias
			if cityAlias == "" {
				continue
			}

			if cityMap[cityAlias] {
				return true
			}
		}
	}

	return false
}

func createCompanyPriority(ukrainianVacancy, reviewExists, photoExists bool, vacanciesWithSalaryCount, vacancyCount int, verifyCompany bool) uint32 {
	result := uint32(0)

	if vacancyCount > 255 {
		vacancyCount = 255
	}
	result |= uint32(vacancyCount)

	if vacanciesWithSalaryCount > 255 {
		vacanciesWithSalaryCount = 255
	}
	result |= uint32(vacanciesWithSalaryCount) << 8

	if ukrainianVacancy {
		result |= 1 << 20
	}
	if reviewExists {
		result |= 1 << 19
	}
	if photoExists {
		result |= 1 << 18
	}

	// all vacancies with salary
	allVacanciesWithSalary := vacancyCount == vacanciesWithSalaryCount
	if allVacanciesWithSalary {
		result |= 1 << 17
	}

	// self verified with all rules
	if ukrainianVacancy && reviewExists && photoExists && allVacanciesWithSalary && verifyCompany {
		result |= 1 << 16
	}

	return result
}
