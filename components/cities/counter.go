package cities

type Counter struct {
	nameMap map[string]bool
}

func NewCounter() *Counter {
	return &Counter{
		nameMap: map[string]bool{},
	}
}

func (c *Counter) Add(name string) {
	c.nameMap[name] = true
}

func (c *Counter) Count() int {
	return len(c.nameMap)
}

func (c *Counter) Reset() {
	for name := range c.nameMap {
		delete(c.nameMap, name)
	}
}
