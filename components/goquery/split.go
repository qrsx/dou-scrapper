package scrapper

import "strings"

func Split(s string) []string {
	items := strings.Split(s, ",")

	result := items[:0]

	for _, item := range items {
		attempt := strings.TrimSpace(item)

		if attempt != "" {
			result = append(result, attempt)
		}
	}

	return result
}
