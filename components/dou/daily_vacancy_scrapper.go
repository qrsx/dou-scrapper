package dou

import (
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"time"
)

type DailyVacancyScrapper struct {
	main *VacancyScrapper
	day  time.Time
}

func NewDailyVacancyScrapper(main *VacancyScrapper) *DailyVacancyScrapper {
	return &DailyVacancyScrapper{
		main: main,
		day:  time.Unix(0, 0),
	}
}

func (s *DailyVacancyScrapper) Scrap() ([]dou.PaginationVacancy, dou.ScrapStats, error) {
	now := time.Now()

	if s.day.Day() == now.Day() {
		return s.main.DiffScrap()
	}

	s.day = now

	return s.main.FullScrap()
}
