package dou

//easyjson:json
type PaginationResponse struct {
	HTML string `json:"html"`
	Last bool   `json:"last"`
	Num  int    `json:"num"`
}
