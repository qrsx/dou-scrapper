package dou

// source https://s.dou.ua/files/lenta/top50-jul19/top50-jul-2019-script.js?v3
var (
	top50LargestCompanies = []string{
		"epam-systems",
		"softserve",
		"luxoft",
		"globallogic",
		"ciklum",
		"infopulse",
		"nix-solutions-ltd",
		"dataart",
		"eleks",
		"evoplay",
		"zone3000",
		"netcracker",
		"intellias",
		"lucky-labs",
		"sigma-software",
		"evo",
		"n-ix",
		"miratech",
		"lohika-systems",
		"playtika-ua",
		"plarium",
		"isd",
		"intecracy-group-consortium",
		"geeksforless",
		"astound",
		"terrasoft",
		"samsung",
		"playtech",
		"nexteum",
		"gameloft",
		"innovecs",
		"korporatsiya-parus",
		"svitla-systems-inc",
		"ring-ukraine",
		"onseo",
		"oracle",
		"tapmedia",
		"provectus",
		"genesis-technology-partners",
		"amcbridge",
		"star",
		"dev-pronet",
		"cs-ltd",
		"depositphotos",
		"wargaming",
		"templatemonster",
		"corevalue",
		"netpeak",
		"delphi-llc",
		"intetics-co",
		"g5-entertainment-ab",
		"daxx-group",
		"tickets-ua",
		"intetics-co",
		"autodoc",
		"ubisoft",
		"nix-solutions-ltd",
	}
)

func Top50LargestCompanyAliasMap() map[string]bool {
	result := make(map[string]bool, len(top50LargestCompanies))

	for _, alias := range top50LargestCompanies {
		result[alias] = true
	}

	return result
}
