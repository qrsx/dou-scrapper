package gmap

type ScrapStats struct {
	RequestCount int
	NewestCount  int
	ErrorCount   int
}
