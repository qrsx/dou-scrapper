package dou

type SkipCompanySortList []SkipCompany

func (l SkipCompanySortList) Len() int {
	return len(l)
}

func (l SkipCompanySortList) Less(i, j int) bool {
	iPriority := l[i].VacancyCount
	jPriority := l[j].VacancyCount

	return iPriority >= jPriority
}

func (l SkipCompanySortList) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}
