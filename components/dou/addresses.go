package dou

import "gitlab.com/qrsx/dou-scrapper/models/dou"

const (
	expectAddressCount = 2048
)

// map[string]dou.Company only as source, to skip convert to slice
func AddressesByCompanies(companies map[string]dou.Company, skipAddressMap map[string]bool) []string {
	result := make([]string, 0, expectAddressCount)

	for _, company := range companies {
		for _, office := range company.Offices {
			address, skip := addressByOffice(office)
			if skip {
				continue
			}

			if skipAddressMap[address] {
				continue
			}

			result = append(result, address)
		}
	}

	return result
}

func addressByOffice(office dou.Office) (string, bool) {
	if office.Address == "" || addressSkipMap[office.Address] {
		return "", true
	}

	address := gmapAddress(office)

	if replaceAddress, exists := addressReplaceMap[address]; exists {
		address = replaceAddress
	}

	return address, false
}

func gmapAddress(office dou.Office) string {
	return office.Address + "," + office.City
}
