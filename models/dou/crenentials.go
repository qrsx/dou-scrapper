package dou

type Credentials struct {
	UserAgent               string
	CookieCSFRToken         string
	BodyCSFRMiddlewareToken string
}

func (c Credentials) IsEmpty() bool {
	var empty = Credentials{}

	return c == empty
}
