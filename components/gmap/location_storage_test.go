package gmap

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/qrsx/dou-scrapper/models/gmap"
	"os"
	"path/filepath"
	"testing"
)

const (
	locationStoragePath = "/var/proto.storage/gmap/location-test.storage"
)

func TestLocationStorage_FetchNotExists(t *testing.T) {
	if testing.Short() {
		return
	}

	path, err := filepath.Abs(locationStoragePath)
	if !assert.NoError(t, err) {
		return
	}

	err = os.Remove(path)
	if err != nil && !os.IsNotExist(err) {
		assert.NoError(t, err)

		return
	}

	storage := NewLocationStorage(path)

	data, err := storage.Fetch()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, map[string]gmap.Location{}, data)
}

func TestLocationStorage_Store(t *testing.T) {
	if testing.Short() {
		return
	}

	path, err := filepath.Abs(locationStoragePath)
	if !assert.NoError(t, err) {
		return
	}

	storage := NewLocationStorage(path)

	// store empty
	err = storage.Store(nil)
	if !assert.NoError(t, err) {
		return
	}

	expected := map[string]gmap.Location{
		"вулиця Шота Руставелі, 19, Київ": {
			Latitude:  50.438354,
			Longitude: 30.5173603,
		},
	}

	err = storage.Store(expected)
	if !assert.NoError(t, err) {
		return
	}

	actual, err := storage.Fetch()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, expected, actual)
}
