package dou

type ScrapStats struct {
	RequestCount int
	NewestCount  int
	ErrorCount   int
}

type AnalyzeStats struct {
	TotalCount  int
	NewestCount int
	ErrorCount  int
}

func (sum *ScrapStats) Merge(value ScrapStats) {
	sum.RequestCount += value.RequestCount
	sum.NewestCount += value.NewestCount
	sum.ErrorCount += value.ErrorCount
}

func (sum *ScrapStats) MergeAnalyze(value AnalyzeStats) {
	sum.NewestCount += value.NewestCount
	sum.ErrorCount += value.ErrorCount
}
