package dou

import (
	"fmt"
	"github.com/juju/errors"
	"github.com/valyala/fasthttp"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"net/http"
	"strconv"
)

const (
	companiesUrl = "https://jobs.dou.ua/vacancies/"
	reviewsUrl   = "https://jobs.dou.ua/reviews/"
)

type Client struct {
	httpClient  *fasthttp.Client
	credentials dou.Credentials
}

func NewClient(httpClient *fasthttp.Client, credentials dou.Credentials) *Client {
	return &Client{httpClient: httpClient, credentials: credentials}
}

func (c *Client) VacanciesMainPage() ([]byte, error) {
	return c.fetch(companiesUrl)
}

func (c *Client) VacancyPagination(count int) ([]byte, error) {
	request := fasthttp.AcquireRequest()
	response := fasthttp.AcquireResponse()
	defer func() {
		fasthttp.ReleaseRequest(request)
		fasthttp.ReleaseResponse(response)
	}()

	request.SetRequestURI("https://jobs.dou.ua/vacancies/xhr-load/?")
	request.SetBody([]byte(fmt.Sprintf("csrfmiddlewaretoken=%s&count=%d", c.credentials.BodyCSFRMiddlewareToken, count)))
	request.Header.SetMethod("POST")
	request.Header.SetUserAgent(c.credentials.UserAgent)
	request.Header.SetContentType("application/x-www-form-urlencoded; charset=UTF-8")
	request.Header.Set("Accept", "application/json, text/javascript, */*; q=0.01")
	request.Header.Set("Accept-Language", "uk")
	request.Header.Set("Referer", companiesUrl)
	request.Header.Set("Connection", "keep-alive")
	request.Header.Set("Cookie", fmt.Sprintf("csrftoken=%s", c.credentials.CookieCSFRToken))

	return c.do(request, response)
}

func (c *Client) ReviewsMainPage() ([]byte, error) {
	return c.fetch(reviewsUrl)
}

func (c *Client) ReviewsByPage(page int) ([]byte, error) {
	return c.fetch(reviewsUrl + strconv.Itoa(page) + "/")
}

func (c *Client) CompanyOfficesByAlias(alias string) ([]byte, error) {
	return c.fetch("https://jobs.dou.ua/companies/" + alias + "/offices/")
}

func (c *Client) fetch(url string) ([]byte, error) {
	request := fasthttp.AcquireRequest()
	response := fasthttp.AcquireResponse()
	defer func() {
		fasthttp.ReleaseRequest(request)
		fasthttp.ReleaseResponse(response)
	}()

	request.SetRequestURI(url)
	request.Header.SetMethod("GET")
	request.Header.SetUserAgent(c.credentials.UserAgent)
	request.Header.Set("Accept-Language", "uk")
	request.Header.Set("Cookie", fmt.Sprintf("csrftoken=%s", c.credentials.CookieCSFRToken))

	return c.do(request, response)
}

func (c *Client) do(request *fasthttp.Request, response *fasthttp.Response) ([]byte, error) {
	err := c.httpClient.Do(request, response)
	if err != nil {
		return nil, err
	}

	statusCode := response.StatusCode()
	content := response.Body()
	if statusCode != http.StatusOK {
		return nil, errors.Trace(fmt.Errorf("unexpected http status code %d, %s", statusCode, content))
	}

	// response.Body() reuse, so need copy
	contentCopy := make([]byte, len(content))
	copy(contentCopy, content)

	return contentCopy, nil
}
