package dou

import (
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"strings"
)

var employeeCountReplaceMap = map[string]string{
	"81...200 спеціалістів":   "81—200",
	"200...800 спеціалістів":  "200—800",
	"800...1500 спеціалістів": "800—1500",
	"понад 1500 спеціалістів": "1500+",
}

func analyzeCompany(companyAlias string, document *goquery.Document) (dou.Company, error) {
	companyInfoSelection := document.Find(".company-info")

	companySiteUrl, _ := companyInfoSelection.Find(".site a").Attr("href")
	companyName := strings.TrimSpace(companyInfoSelection.Find(".g-h2").Text())
	logo, _ := companyInfoSelection.Find(".logo").Attr("src")

	employeeCount := parseEmployeeCount(companyInfoSelection)

	return dou.Company{
		Alias:         companyAlias,
		Name:          companyName,
		Logo:          logo,
		EmployeeCount: employeeCount,
		SiteUrl:       companySiteUrl,
		Offices:       parseOfficesByCities(document.Find(".city")),
		PhotoExists:   parsePhotoExists(document),
	}, nil
}

func parseOfficesByCities(cities *goquery.Selection) []dou.Office {
	offices := make([]dou.Office, 0, 1)

	cities.Each(func(i int, citySelection *goquery.Selection) {
		cityName := citySelection.Find("h4").Text()

		citySelection.Find(".address").Each(func(i int, addressSelection *goquery.Selection) {
			// store city with empty address too
			offices = append(offices, dou.Office{
				City:    cityName,
				Address: getOfficeAddress(addressSelection),
			})
		})
	})

	return offices
}

func getOfficeAddress(addressSelection *goquery.Selection) string {
	source, err := addressSelection.Html()

	if err != nil {
		return ""
	}

	index := strings.Index(source, "<span>")

	if index == -1 {
		return ""
	}

	return strings.TrimSpace(source[:index])
}

func parseEmployeeCount(selection *goquery.Selection) string {
	text := selection.Text()

	for search, result := range employeeCountReplaceMap {
		if strings.Contains(text, search) {
			return result
		}
	}

	return ""
}

func parsePhotoExists(document *goquery.Document) bool {
	links := document.Find(".company-nav li a")

	result := false

	links.Each(func(i int, link *goquery.Selection) {
		if result {
			return
		}

		url, ok := link.Attr("href")

		result = ok && strings.HasSuffix(url, "/photos/")
	})

	return result
}
