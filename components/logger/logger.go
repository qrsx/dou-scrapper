package logger

import (
	"log"
)

const (
	i = "\x1b[92m[INFO]\x1b[0m"
	e = "\x1b[91m[ERROR]\x1b[0m"
	c = "\x1b[91m[CRITICAL]\x1b[0m"
)

func Info(a string) {
	log.Print(i + " " + a)
}

func Infof(format string, a ...interface{}) {
	log.Printf(i+" "+format, a...)
}

func Error(err error) {
	Errorf("%+v", err)
}

func Errorf(format string, a ...interface{}) {
	log.Printf(e+" "+format, a...)
}

func Critical(err error) {
	Criticalf("%+v", err)
}

func Criticalf(format string, a ...interface{}) {
	log.Fatalf(c+" "+format, a...)
}
