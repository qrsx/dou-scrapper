package gmap

import (
	"gitlab.com/qrsx/dou-scrapper/models/gmap"
	"gitlab.com/qrsx/dou-scrapper/models/protos/gmap/storage"
	"io/ioutil"
	"os"
)

type LocationStorage struct {
	path string
}

func NewLocationStorage(path string) *LocationStorage {
	return &LocationStorage{path: path}
}

func (s *LocationStorage) Fetch() (map[string]gmap.Location, error) {
	content, err := ioutil.ReadFile(s.path)

	if err != nil {
		if os.IsNotExist(err) {
			return map[string]gmap.Location{}, nil
		}

		return nil, err

	}

	message := storage.AddressLocationList{}

	err = message.Unmarshal(content)
	if err != nil {
		return nil, err
	}

	locations := message.Items
	result := make(map[string]gmap.Location, len(locations))
	for _, addressLocation := range locations {
		result[addressLocation.Address] = gmap.Location{
			Latitude:  addressLocation.Location.Latitude,
			Longitude: addressLocation.Location.Longitude,
		}
	}

	return result, nil
}

func (s *LocationStorage) Store(source map[string]gmap.Location) error {
	addressLocationList := make([]*storage.AddressLocation, 0, len(source))
	for address, location := range source {
		addressLocationList = append(addressLocationList, &storage.AddressLocation{
			Address: address,
			Location: &storage.Location{
				Latitude:  location.Latitude,
				Longitude: location.Longitude,
			},
		})
	}

	result := storage.AddressLocationList{
		Items: addressLocationList,
	}

	content, err := result.Marshal()

	if err != nil {
		return err
	}

	return ioutil.WriteFile(s.path, content, 0666)
}
