package gmap

type (
	ResponseLocation struct {
		Latitude  float64 `json:"lat"`
		Longitude float64 `json:"lng"`
	}

	ResponseGeometry struct {
		Location ResponseLocation `json:"location"`
	}

	ResponseResults struct {
		Geometry ResponseGeometry `json:"geometry"`
	}
)

// easyjson:json
type Response struct {
	Result []ResponseResults `json:"results"`
}

/*
{
   "results" : [
      {
         "address_components" : [
            {
               "long_name" : "19",
               "short_name" : "19",
               "types" : [ "street_number" ]
            },
            {
               "long_name" : "Shota Rustaveli Street",
               "short_name" : "Shota Rustaveli St",
               "types" : [ "route" ]
            },
            {
               "long_name" : "Pechers'kyi district",
               "short_name" : "Pechers'kyi district",
               "types" : [ "political", "sublocality", "sublocality_level_1" ]
            },
            {
               "long_name" : "Kyiv",
               "short_name" : "Kyiv",
               "types" : [ "locality", "political" ]
            },
            {
               "long_name" : "Kyiv City",
               "short_name" : "Kyiv City",
               "types" : [ "administrative_area_level_2", "political" ]
            },
            {
               "long_name" : "Ukraine",
               "short_name" : "UA",
               "types" : [ "country", "political" ]
            },
            {
               "long_name" : "02000",
               "short_name" : "02000",
               "types" : [ "postal_code" ]
            }
         ],
         "formatted_address" : "Shota Rustaveli St, 19, Kyiv, Ukraine, 02000",
         "geometry" : {
            "location" : {
               "lat" : 50.438354,
               "lng" : 30.519549
            },
            "location_type" : "ROOFTOP",
            "viewport" : {
               "northeast" : {
                  "lat" : 50.4397029802915,
                  "lng" : 30.5208979802915
               },
               "southwest" : {
                  "lat" : 50.4370050197085,
                  "lng" : 30.5182000197085
               }
            }
         },
         "place_id" : "ChIJZ9gJY_7O1EARAHRdVuzI_Os",
         "plus_code" : {
            "compound_code" : "CGQ9+8R Kyiv, Kyiv city, Ukraine",
            "global_code" : "9G2GCGQ9+8R"
         },
         "types" : [ "street_address" ]
      }
   ],
   "status" : "OK"
}
*/
