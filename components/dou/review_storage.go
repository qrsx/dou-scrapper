package dou

import (
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"gitlab.com/qrsx/dou-scrapper/models/protos/dou/storage"
	"io/ioutil"
	"os"
	"time"
)

type ReviewStorage struct {
	path     string
	lifetime uint32
}

func NewReviewStorage(path string, lifetime uint32) *ReviewStorage {
	return &ReviewStorage{path: path, lifetime: lifetime}
}

func (s *ReviewStorage) Fetch() ([]dou.PaginationReview, bool, error) {
	content, err := ioutil.ReadFile(s.path)

	if err != nil {
		if os.IsNotExist(err) {
			return nil, false, nil
		}

		return nil, false, err

	}

	message := storage.Reviews{}

	err = message.Unmarshal(content)
	if err != nil {
		return nil, false, err
	}

	expired := uint32(time.Now().Unix())-s.lifetime > message.Updated
	reviews := message.Reviews
	result := make([]dou.PaginationReview, len(reviews))
	for i, v := range reviews {
		result[i] = dou.PaginationReview{
			Url:          v.Url,
			CompanyAlias: v.CompanyAlias,
		}
	}

	return result, expired, nil
}

func (s *ReviewStorage) Store(source []dou.PaginationReview) error {
	reviews := make([]*storage.Review, len(source))
	for i, v := range source {
		reviews[i] = &storage.Review{
			Url:          v.Url,
			CompanyAlias: v.CompanyAlias,
		}
	}

	result := storage.Reviews{
		Reviews: reviews,
		Updated: uint32(time.Now().Unix()),
	}

	content, err := result.Marshal()

	if err != nil {
		return err
	}

	return ioutil.WriteFile(s.path, content, 0666)
}
