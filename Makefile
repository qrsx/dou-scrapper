install:
	mkdir -p projects

	sudo mkdir -p /var/proto.storage/dou
	sudo chmod -R 777 /var/proto.storage/dou

	sudo mkdir -p /var/proto.storage/gmap
	sudo chmod -R 777 /var/proto.storage/gmap

clear:
	rm /var/proto.storage/dou/vacancy-test.storage
	rm /var/proto.storage/dou/review-test.storage

fmt:
	go fmt ./components/... ./models/... ./cmd/...

test:
	go test ./components/... -v -bench=. -benchmem

gogofaster:
	go get github.com/gogo/protobuf/protoc-gen-gogofaster

proto:
	protoc -I . protos/dou/storage/*.proto --gogofaster_out=models
	protoc -I . protos/gmap/storage/*.proto --gogofaster_out=models

easyjson:
	easyjson ./models/dou/pagination_response.go \
	    ./models/gmap/geocode.go

run:
	go run ./cmd/scrapper/*.go