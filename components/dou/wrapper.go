package dou

const (
	ot = "<div>"
	ct = "</div>"
)

type Wrapper struct {
	data []byte
}

func (w *Wrapper) Wrap(s []byte) []byte {
	result := append(w.data[:0], []byte(ot)...)
	result = append(result, s...)
	result = append(result, []byte(ct)...)

	// for reuse memory
	w.data = result

	return result
}
