package dou

import (
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"strings"
)

var remoteMap = map[string]bool{
	"удаленно":  true,
	"Удаленно":  true,
	"віддалено": true,
	"Віддалено": true,
}

func extractRemoteCity(cities []string) ([]string, bool) {
	result := cities[:0]

	remote := false
	for _, city := range cities {
		if remoteMap[city] {
			remote = true
		} else {
			result = append(result, city)
		}
	}

	return result, remote
}

func inCityMap(cityExistsMap map[string]bool, cities []string) bool {
	for _, city := range cities {
		if cityExistsMap[strings.ToLower(city)] {
			return true
		}
	}

	return false
}

func createOfficeCityMap(offices []dou.FullOffice) map[string]bool {
	result := make(map[string]bool, len(offices))

	for j := range offices {
		result[strings.ToLower(offices[j].City.Name)] = true
	}

	return result
}
