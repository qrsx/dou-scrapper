package scrapper

import (
	"bytes"
	"github.com/PuerkitoBio/goquery"
)

func Document(content []byte) (*goquery.Document, error) {
	return goquery.NewDocumentFromReader(bytes.NewReader(content))
}
