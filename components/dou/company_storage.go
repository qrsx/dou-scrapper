package dou

import (
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"gitlab.com/qrsx/dou-scrapper/models/protos/dou/storage"
	"io/ioutil"
	"os"
	"time"
)

type CompanyStorage struct {
	path     string
	lifetime uint32
}

func NewCompanyStorage(path string, lifetime uint32) *CompanyStorage {
	return &CompanyStorage{path: path, lifetime: lifetime}
}

func (s *CompanyStorage) Fetch() ([]dou.Company, error) {
	content, err := ioutil.ReadFile(s.path)

	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}

		return nil, err

	}

	message := storage.Companies{}

	err = message.Unmarshal(content)
	if err != nil {
		return nil, err
	}

	expired := uint32(time.Now().Unix()) - s.lifetime
	companies := message.Companies
	result := make([]dou.Company, len(companies))
	for i, v := range companies {
		result[i] = dou.Company{
			Alias:         v.Alias,
			Name:          v.Name,
			Logo:          v.Logo,
			SiteUrl:       v.SiteUrl,
			EmployeeCount: v.EmployeeCount,
			Offices:       s.fromProtoOffices(v.Offices),
			PhotoExists:   v.PhotoExists,
			Updated:       v.Updated,
			Actual:        v.Updated > expired,
		}
	}

	return result, nil
}

func (s *CompanyStorage) Store(source []dou.Company) error {
	companies := make([]*storage.Company, len(source))
	for i, v := range source {
		companies[i] = &storage.Company{
			Alias:         v.Alias,
			Name:          v.Name,
			Logo:          v.Logo,
			SiteUrl:       v.SiteUrl,
			EmployeeCount: v.EmployeeCount,
			Offices:       s.toProtoOffices(v.Offices),
			PhotoExists:   v.PhotoExists,
			Updated:       v.Updated,
		}
	}

	result := storage.Companies{
		Companies: companies,
	}

	content, err := result.Marshal()

	if err != nil {
		return err
	}

	return ioutil.WriteFile(s.path, content, 0666)
}

func (s CompanyStorage) fromProtoOffices(from []*storage.Office) []dou.Office {
	result := make([]dou.Office, len(from))

	for i, office := range from {
		result[i] = dou.Office{
			City:    office.City,
			Address: office.Address,
		}
	}

	return result
}

func (s CompanyStorage) toProtoOffices(from []dou.Office) []*storage.Office {
	result := make([]*storage.Office, len(from))

	for i, office := range from {
		result[i] = &storage.Office{
			City:    office.City,
			Address: office.Address,
		}
	}

	return result
}
