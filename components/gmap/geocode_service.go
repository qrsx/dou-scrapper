package gmap

import (
	"github.com/juju/errors"
	"gitlab.com/qrsx/dou-scrapper/components/logger"
	"gitlab.com/qrsx/dou-scrapper/models/gmap"
)

type GeocodeService struct {
	storage *LocationStorage
	client  *Client
}

func NewGeocodeService(storage *LocationStorage, client *Client) *GeocodeService {
	return &GeocodeService{storage: storage, client: client}
}

func (s *GeocodeService) FetchByAddresses(addresses []string) (map[string]gmap.Location, []string, gmap.ScrapStats, error) {
	stats := gmap.ScrapStats{}

	addressLocationMap, err := s.storage.Fetch()

	if err != nil {
		return nil, nil, stats, errors.Trace(err)
	}

	result := make(map[string]gmap.Location, len(addresses))
	invalidAddresses := make([]string, 0)

	for _, address := range addresses {
		if location, exists := addressLocationMap[address]; exists {
			result[address] = location

			continue
		}

		location, err := s.client.FetchByAddress(address)
		stats.RequestCount += 1

		if err != nil {
			stats.ErrorCount += 1
			invalidAddresses = append(invalidAddresses, address)

			logger.Errorf("for %s %+v", address, errors.Trace(err))

			continue
		}

		stats.NewestCount += 1
		addressLocationMap[address] = location
		result[address] = location
	}

	err = s.storage.Store(addressLocationMap)
	if err != nil {
		logger.Error(errors.Trace(err))

		// NOP
	}

	return result, invalidAddresses, stats, nil
}
