package cities

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFindCityByName(t *testing.T) {
	assert.Equal(t, Kyiv, FindCityByName("Київ"))
}

func BenchmarkFindCityByName(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = FindCityByName("Київ")
	}
}
