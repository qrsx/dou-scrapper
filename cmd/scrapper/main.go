package main

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/qrsx/dou-scrapper/components/dou"
	"gitlab.com/qrsx/dou-scrapper/components/gmap"
	"gitlab.com/qrsx/dou-scrapper/components/logger"
	. "gitlab.com/qrsx/dou-scrapper/models/dou"
	"time"
)

var (
	scrapperCredentials Credentials
	gmapApiKey          string
)

const companyRequestLimit = 100

const (
	vacancyStoragePath  = "/var/proto.storage/dou/vacancy.storage"
	companyStoragePath  = "/var/proto.storage/dou/company.storage"
	reviewStoragePath   = "/var/proto.storage/dou/review.storage"
	locationStoragePath = "/var/proto.storage/gmap/location.storage"
)

func main() {
	if scrapperCredentials.IsEmpty() {
		logger.Criticalf("[DOU] credentials is empty, please fill")
	}

	if gmapApiKey == "" {
		logger.Criticalf("[GMAP] api key is empty, please fill")
	}

	startTime := time.Now()
	combine()
	runDuration := time.Since(startTime)

	logger.Infof("[SCRAPPER] total duration %.3f", float64(runDuration)/float64(time.Second))
}

func combine() {
	httpClient := &fasthttp.Client{}

	douClient := dou.NewClient(httpClient, scrapperCredentials)

	vacancyScrapper := dou.NewVacancyScrapper(
		dou.NewVacancyStorage(vacancyStoragePath, 1800),
		douClient,
	)

	startTime := time.Now()
	vacancies, stats, err := vacancyScrapper.DiffScrap()
	runDuration := time.Since(startTime)

	logger.Infof("[DOU][VACANCIES] total %d", len(vacancies))
	logger.Infof("[DOU][VACANCIES] stats requests %d, newest count %d, error count %d", stats.RequestCount, stats.NewestCount, stats.ErrorCount)
	logger.Infof("[DOU][VACANCIES] duration %.3f\n\n", float64(runDuration)/float64(time.Second))

	if err != nil {
		logger.Critical(err)
	}

	companyScrapper := dou.NewCompanyScrapper(
		dou.NewCompanyStorage(companyStoragePath, 7*24*3600),
		douClient,
	)

	startTime = time.Now()
	companies, stats, err := companyScrapper.ScrapByAliases(dou.ExtractCompanyAliasesByVacancies(vacancies), companyRequestLimit)
	runDuration = time.Since(startTime)

	logger.Infof("[DOU][COMPANIES] total %d", len(companies))
	logger.Infof("[DOU][COMPANIES] stats requests %d, newest count %d, error count %d", stats.RequestCount, stats.NewestCount, stats.ErrorCount)
	logger.Infof("[DOU][COMPANIES] duration %.3f\n\n", float64(runDuration)/float64(time.Second))

	if err != nil {
		logger.Critical(err)
	}

	reviewScrapper := dou.NewReviewScrapper(
		dou.NewReviewStorage(reviewStoragePath, 3600),
		douClient,
	)

	startTime = time.Now()
	reviews, stats, err := reviewScrapper.DiffScrap()
	runDuration = time.Since(startTime)

	logger.Infof("[DOU][REVIEWS] companies with reviews %d", len(reviews))
	logger.Infof("[DOU][REVIEWS] stats requests %d, newest count %d, error count %d", stats.RequestCount, stats.NewestCount, stats.ErrorCount)
	logger.Infof("[DOU][REVIEWS] duration %.3f\n\n", float64(runDuration)/float64(time.Second))

	if err != nil {
		logger.Critical(err)
	}

	geocodeService := gmap.NewGeocodeService(
		gmap.NewLocationStorage(locationStoragePath),
		gmap.NewClient(httpClient, gmapApiKey),
	)

	startTime = time.Now()
	addressLocationMap, invalidAddresses, gmapStats, err := geocodeService.FetchByAddresses(dou.AddressesByCompanies(companies, nil))
	runDuration = time.Since(startTime)

	logger.Infof("[GMAP] total %d", len(addressLocationMap))
	logger.Infof("[GMAP] invalid addresses %d", len(invalidAddresses))
	logger.Infof("[GMAP] stats requests %d, newest count %d, error count %d", gmapStats.RequestCount, gmapStats.NewestCount, gmapStats.ErrorCount)
	logger.Infof("[GMAP] duration %.3f", float64(runDuration)/float64(time.Second))

	if err != nil {
		logger.Critical(err)
	}
}

func companies() {
	httpClient := &fasthttp.Client{}

	douClient := dou.NewClient(httpClient, scrapperCredentials)

	companyScrapper := dou.NewCompanyScrapper(
		dou.NewCompanyStorage(companyStoragePath, 3600),
		douClient,
	)

	startTime := time.Now()
	companies, stats, err := companyScrapper.ScrapByAliases([]string{"dou"}, companyRequestLimit)
	runDuration := time.Since(startTime)

	logger.Infof("[DOU] total companies %d", len(companies))
	logger.Infof("[DOU] total requests %d, newest count %d, error count %d", stats.RequestCount, stats.NewestCount, stats.ErrorCount)
	logger.Infof("[DOU] total duration %.3f", float64(runDuration)/float64(time.Second))

	if err != nil {
		logger.Critical(err)
	}
}
