package dou

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestExtractRemoteCity(t *testing.T) {
	cities, remote := extractRemoteCity([]string{"Київ", "Львів"})

	assert.Equal(t, []string{"Київ", "Львів"}, cities)
	assert.Equal(t, false, remote)

	cities, remote = extractRemoteCity([]string{"Київ", "Дніпро", "Віддалено"})

	assert.Equal(t, []string{"Київ", "Дніпро"}, cities)
	assert.Equal(t, true, remote)
}
