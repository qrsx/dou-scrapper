package dou

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseVacancyUrl(t *testing.T) {
	url, hot := parseVacancyUrl("https://jobs.dou.ua/companies/devspace/vacancies/100000/?from=list_hot")

	assert.Equal(t, "https://jobs.dou.ua/companies/devspace/vacancies/100000/", url)
	assert.Equal(t, true, hot)

	url, hot = parseVacancyUrl("https://jobs.dou.ua/companies/devspace/vacancies/100000/")

	assert.Equal(t, "https://jobs.dou.ua/companies/devspace/vacancies/100000/", url)
	assert.Equal(t, false, hot)
}

func BenchmarkParseVacancyUrl(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = parseVacancyUrl("https://jobs.dou.ua/companies/devspace/vacancies/100000/?from=list_hot")
	}
}
