package gmap

import (
	"fmt"
	"github.com/juju/errors"
	"github.com/valyala/fasthttp"
	"gitlab.com/qrsx/dou-scrapper/models/gmap"
	"net/http"
)

const (
	gmapPath = `https://maps.googleapis.com/maps/api/geocode/json`
)

var (
	ErrInvalidGeocodeResponse = errors.New("invalid geocode response")
)

type Client struct {
	httpClient *fasthttp.Client
	apiKey     string
}

func NewClient(httpClient *fasthttp.Client, apiKey string) *Client {
	return &Client{httpClient: httpClient, apiKey: apiKey}
}

func (c *Client) FetchByAddress(address string) (gmap.Location, error) {
	request := fasthttp.AcquireRequest()

	request.SetRequestURI(gmapPath)
	request.Header.SetMethod("GET")

	args := request.URI().QueryArgs()
	args.Add("key", c.apiKey)
	args.Add("address", address)

	response := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseRequest(request)
		fasthttp.ReleaseResponse(response)
	}()

	err := c.httpClient.Do(request, response)
	if err != nil {
		return gmap.Location{}, errors.Trace(err)
	}

	statusCode := response.StatusCode()
	content := response.Body()
	if statusCode != http.StatusOK {
		return gmap.Location{}, fmt.Errorf("unexpected http status code %d, %s", statusCode, content)
	}

	gmapResponse := gmap.Response{}
	err = gmapResponse.UnmarshalJSON(content)

	if err != nil {
		return gmap.Location{}, errors.Trace(err)
	}

	if len(gmapResponse.Result) > 0 {
		location := gmapResponse.Result[0].Geometry.Location

		result := gmap.Location{
			Latitude:  location.Latitude,
			Longitude: location.Longitude,
		}

		return result, nil
	}

	return gmap.Location{}, ErrInvalidGeocodeResponse
}
