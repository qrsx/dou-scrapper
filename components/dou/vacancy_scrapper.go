package dou

import (
	"github.com/juju/errors"
	"gitlab.com/qrsx/dou-scrapper/components/goquery"
	"gitlab.com/qrsx/dou-scrapper/components/logger"
	"gitlab.com/qrsx/dou-scrapper/components/roundtime"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
)

const (
	maxExpectedVacancyCount        = 8096
	paginationExpectedVacancyCount = 40
)

type VacancyScrapper struct {
	storage *VacancyStorage
	client  *Client
}

func NewVacancyScrapper(storage *VacancyStorage, client *Client) *VacancyScrapper {
	return &VacancyScrapper{storage: storage, client: client}
}

func (s *VacancyScrapper) FullScrap() ([]dou.PaginationVacancy, dou.ScrapStats, error) {
	stats := dou.ScrapStats{}

	stored, expired, err := s.storage.Fetch()
	if err != nil {
		return nil, stats, err
	}

	if len(stored) > 0 && expired == false {
		return stored, stats, nil
	}

	stats.RequestCount += 1
	response, err := s.client.VacanciesMainPage()
	if err != nil {
		return nil, stats, err
	}

	document, err := scrapper.Document(response)
	if err != nil {
		return nil, stats, err
	}

	buffer := make([]dou.PaginationVacancy, 0, maxExpectedVacancyCount)

	// in this case: stored vacancies use only for "created" date
	urlImportMap := createUrlImportedMap(stored)

	vacancies, analyzeStats := analyzePaginationVacancySet(buffer, urlImportMap, roundtime.Now(), document)
	stats.MergeAnalyze(analyzeStats)

	wrapper := new(Wrapper)
	currentCount := analyzeStats.TotalCount

	for {
		stats.RequestCount += 1
		response, err = s.client.VacancyPagination(currentCount)
		if err != nil {
			return nil, stats, err
		}

		paginationResponse := dou.PaginationResponse{}
		err = paginationResponse.UnmarshalJSON(response)
		if err != nil {
			return nil, stats, err
		}

		document, err = scrapper.Document(wrapper.Wrap([]byte(paginationResponse.HTML)))
		if err != nil {
			return nil, stats, err
		}

		vacancies, analyzeStats = analyzePaginationVacancySet(vacancies, urlImportMap, roundtime.Now(), document)
		stats.MergeAnalyze(analyzeStats)

		if paginationResponse.Last {
			break
		}

		currentCount += paginationResponse.Num
	}

	err = s.storage.Store(vacancies)
	if err != nil {
		logger.Error(errors.Trace(err))

		// NOP, only logged
	}

	return vacancies, stats, nil
}

// scrap only top, hot in top -> skip hots, than running when exists newest
func (s *VacancyScrapper) DiffScrap() ([]dou.PaginationVacancy, dou.ScrapStats, error) {
	stats := dou.ScrapStats{}

	stored, expired, err := s.storage.Fetch()
	if err != nil {
		return nil, stats, err
	}

	if len(stored) > 0 && expired == false {
		return stored, stats, nil
	}

	stats.RequestCount += 1
	response, err := s.client.VacanciesMainPage()
	if err != nil {
		return nil, stats, err
	}

	document, err := scrapper.Document(response)
	if err != nil {
		return nil, stats, err
	}

	buffer := make([]dou.PaginationVacancy, 0, maxExpectedVacancyCount)

	urlImportMap := createUrlImportedMap(stored)

	vacancies, analyzeStats := analyzePaginationVacancySet(buffer, urlImportMap, roundtime.Now(), document)
	stats.MergeAnalyze(analyzeStats)

	wrapper := new(Wrapper)
	currentCount := analyzeStats.TotalCount

	newestBuffer := make([]dou.PaginationVacancy, 0, paginationExpectedVacancyCount)

	for {
		stats.RequestCount += 1
		response, err = s.client.VacancyPagination(currentCount)
		if err != nil {
			return nil, stats, err
		}

		paginationResponse := new(dou.PaginationResponse)
		err = paginationResponse.UnmarshalJSON(response)
		if err != nil {
			return nil, stats, err
		}

		document, err = scrapper.Document(wrapper.Wrap([]byte(paginationResponse.HTML)))
		if err != nil {
			return nil, stats, err
		}

		potentialNewestVacancies, analyzeStats := analyzePaginationVacancySet(newestBuffer, urlImportMap, roundtime.Now(), document)
		stats.MergeAnalyze(analyzeStats)

		if paginationResponse.Last {
			break
		}

		if analyzeStats.NewestCount == 0 && !existsHot(potentialNewestVacancies) {
			break
		}

		vacancies = append(vacancies, potentialNewestVacancies...)
		currentCount += paginationResponse.Num

		// if paginationExpectedVacancyCount increased
		newestBuffer = potentialNewestVacancies[:0]
	}

	vacancies = mergeVacanciesDiff(vacancies, stored)

	err = s.storage.Store(vacancies)
	if err != nil {
		logger.Error(errors.Trace(err))

		// NOP, only logged
	}

	return vacancies, stats, nil
}

func createUrlImportedMap(items []dou.PaginationVacancy) map[string]uint32 {
	result := make(map[string]uint32, len(items))

	for _, item := range items {
		result[item.Url] = item.Imported
	}

	return result
}

func existsHot(items []dou.PaginationVacancy) bool {
	for _, item := range items {
		if item.Hot {
			return true
		}
	}

	return false
}

func mergeVacanciesDiff(newestOrHot, stored []dou.PaginationVacancy) []dou.PaginationVacancy {
	result := newestOrHot

	existsMap := make(map[string]bool, len(newestOrHot))
	for _, item := range newestOrHot {
		existsMap[item.Url] = true
	}

	for _, item := range stored {
		if !existsMap[item.Url] {
			result = append(result, item)
		}
	}

	return result
}
