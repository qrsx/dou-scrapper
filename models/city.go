package models

type City struct {
	Alias string
	Name  string
}
