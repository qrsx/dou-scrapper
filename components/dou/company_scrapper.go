package dou

import (
	"github.com/juju/errors"
	"gitlab.com/qrsx/dou-scrapper/components/goquery"
	"gitlab.com/qrsx/dou-scrapper/components/logger"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"time"
)

type CompanyScrapper struct {
	storage *CompanyStorage
	client  *Client
}

func NewCompanyScrapper(storage *CompanyStorage, client *Client) *CompanyScrapper {
	return &CompanyScrapper{storage: storage, client: client}
}

func (s *CompanyScrapper) ScrapByAliases(aliases []string, softRequestLimit int) (map[string]dou.Company, dou.ScrapStats, error) {
	stats := dou.ScrapStats{}

	stored, err := s.storage.Fetch()
	if err != nil {
		return nil, stats, err
	}

	length := len(aliases)
	result := make(map[string]dou.Company, length)
	newestAliases := make([]string, 0, length)
	expiredAliases := make([]string, 0, length)

	aliasCompanyMap := createAliasCompanyMapBySlice(stored)

	now := uint32(time.Now().Unix())
	for _, alias := range aliases {
		company, exists := aliasCompanyMap[alias]

		if exists {
			if company.Actual {
				result[alias] = company
			} else {
				expiredAliases = append(expiredAliases, alias)
			}
		} else {
			newestAliases = append(newestAliases, alias)
		}
	}

	// newest company requests without limit
	newestScrapStats := s.scrap(result, aliasCompanyMap, newestAliases, now)
	stats.Merge(newestScrapStats)

	// fill expired as default
	for _, alias := range expiredAliases {
		result[alias] = aliasCompanyMap[alias]
	}

	restRequestLimit := softRequestLimit - stats.RequestCount
	if restRequestLimit > 0 {
		length := len(expiredAliases)

		if restRequestLimit > length {
			restRequestLimit = length
		}

		expiredScrapStats := s.scrap(result, aliasCompanyMap, expiredAliases[:restRequestLimit], now)
		stats.Merge(expiredScrapStats)
	}

	err = s.storage.Store(createCompanySliceByAliasMap(aliasCompanyMap))
	if err != nil {
		logger.Error(errors.Trace(err))

		// NOP, only logged
	}

	return result, stats, nil
}

func (s *CompanyScrapper) scrap(result, aliasCompanyMap map[string]dou.Company, aliases []string, now uint32) dou.ScrapStats {
	stats := dou.ScrapStats{}

	for _, alias := range aliases {
		companyOfficesContent, err := s.client.CompanyOfficesByAlias(alias)
		stats.RequestCount += 1

		if err != nil {
			logger.Error(errors.Trace(err))

			stats.ErrorCount += 1

			continue
		}

		document, err := scrapper.Document(companyOfficesContent)
		if err != nil {
			logger.Error(errors.Trace(err))

			stats.ErrorCount += 1

			continue
		}

		company, err := analyzeCompany(alias, document)
		if err != nil {
			logger.Error(errors.Trace(err))

			stats.ErrorCount += 1

			continue
		}

		company.Updated = now
		result[alias] = company

		if _, existsCompany := aliasCompanyMap[alias]; !existsCompany {
			stats.NewestCount += 1
		}

		aliasCompanyMap[alias] = company
	}

	return stats
}

func createAliasCompanyMapBySlice(companies []dou.Company) map[string]dou.Company {
	result := make(map[string]dou.Company, len(companies))

	for _, company := range companies {
		result[company.Alias] = company
	}

	return result
}

func createCompanySliceByAliasMap(aliasMap map[string]dou.Company) []dou.Company {
	result := make([]dou.Company, len(aliasMap))

	for _, company := range aliasMap {
		result = append(result, company)
	}

	return result
}
