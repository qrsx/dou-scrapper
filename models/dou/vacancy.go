package dou

type PaginationVacancy struct {
	Url      string
	Title    string
	Salary   string
	Cities   []string
	Imported uint32
	Remote   bool
	Hot      bool
}
