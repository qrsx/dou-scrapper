package dou

type Office struct {
	City    string
	Address string
}

type Company struct {
	Alias         string
	Name          string
	Logo          string
	SiteUrl       string
	EmployeeCount string
	Offices       []Office
	PhotoExists   bool
	Updated       uint32
	Actual        bool
}
