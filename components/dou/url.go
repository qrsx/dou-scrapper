package dou

import (
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"strings"
)

func ExtractCompanyAliasesByVacancies(vacancies []dou.PaginationVacancy) []string {
	length := len(vacancies)

	aliasExists := make(map[string]bool, length)
	aliases := make([]string, 0, length)

	for i := range vacancies {
		url := vacancies[i].Url

		alias := extractCompanyAlias(url)

		if aliasExists[alias] {
			continue
		}
		aliasExists[alias] = true

		aliases = append(aliases, alias)
	}

	return aliases
}

func parseVacancyUrl(s string) (string, bool) {
	startIndex := strings.Index(s, "?")

	if startIndex == -1 {
		return s, false
	}

	return s[:startIndex], s[startIndex+1:] == "from=list_hot"
}

func extractCompanyAlias(fullUrl string) string {
	return extractAlias(fullUrl, "/companies/")
}

func extractVacancyAlias(fullUrl string) string {
	return extractAlias(fullUrl, "/vacancies/")
}

func extractAlias(fullUrl, path string) string {
	startIndex := strings.Index(fullUrl, path)

	rest := fullUrl[startIndex+len(path):]

	endIndex := strings.Index(rest, "/")

	return rest[:endIndex]
}
