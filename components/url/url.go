package url

import "strings"

func RemoveQuery(full string) string {
	startIndex := strings.Index(full, "?")

	if startIndex == -1 {
		return full
	}

	return full[:startIndex]
}
