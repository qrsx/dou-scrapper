package dou

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/qrsx/dou-scrapper/models/dou"
	"os"
	"path/filepath"
	"testing"
)

const (
	reviewStoragePath = "/var/proto.storage/dou/review-test.storage"
)

func TestReviewStorage_FetchNotExist(t *testing.T) {
	if testing.Short() {
		return
	}

	path, err := filepath.Abs(reviewStoragePath)
	if !assert.NoError(t, err) {
		return
	}

	err = os.Remove(path)
	if err != nil && !os.IsNotExist(err) {
		assert.NoError(t, err)

		return
	}

	storage := NewReviewStorage(path, 3600)

	data, expired, err := storage.Fetch()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, []dou.PaginationReview(nil), data)
	assert.Equal(t, false, expired)
}

func TestReviewStorage_Store(t *testing.T) {
	if testing.Short() {
		return
	}

	path, err := filepath.Abs(reviewStoragePath)
	if !assert.NoError(t, err) {
		return
	}

	storage := NewReviewStorage(path, 3600)

	// store empty
	err = storage.Store(nil)
	if !assert.NoError(t, err) {
		return
	}

	reviews := []dou.PaginationReview{
		{
			Url:          "#1",
			CompanyAlias: "devspace",
		},
		{
			Url:          "#2",
			CompanyAlias: "devspace",
		},
	}

	// store any
	err = storage.Store(reviews)
	if !assert.NoError(t, err) {
		return
	}

	// assert exists
	data, expired, err := storage.Fetch()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, reviews, data)
	assert.Equal(t, false, expired)
}
