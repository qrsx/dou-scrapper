package dou

import "sort"

type VacancySalaryExistsSortList []PaginationVacancy

func NewVacancySalaryExistsSortList(data []PaginationVacancy) sort.Interface {
	return VacancySalaryExistsSortList(data)
}

func (l VacancySalaryExistsSortList) Len() int {
	return len(l)
}

func (l VacancySalaryExistsSortList) Less(i, j int) bool {
	return l[j].Salary == ""
}

func (v VacancySalaryExistsSortList) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}
